## How to Use

__Install__
1. `npm run install`
2. `npx pod-install` to install RNDateTimePicker (3.0.9)

__Run iOS__
1. `npm start`
2. `npm run ios`

__Run Android__
1. `npm run android`
_or_
1. open android studio
2. open android virtual device manager
3. create new virtual device or wipe data existing virtual device
4. launch the emulator
5. `npm run android` (if there is 'no emulators found' error run `source ~/.bash_profile` on terminal first)

## Screenshots of Application
<img src="./screenshots/ios-first-question.png" width="200">  
<img src="./screenshots/ios-alert.png" width="200">  
<img src="./screenshots/ios-complete-summary.png" width="200">  
<img src="./screenshots/ios-summary.png" width="200">

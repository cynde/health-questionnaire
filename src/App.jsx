import React from 'react';
import {SafeAreaView, StatusBar, StyleSheet} from 'react-native';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import Questionnaire from './Screen/Questionnaire/Questionnaire';
import formReducer from './Redux/Reducer';

const globalStore = createStore(formReducer);

const App = () => {
  return (
    <Provider store={globalStore}>
      <StatusBar barStyle={'dark-content'} />
      <SafeAreaView style={styles.container}>
        <Questionnaire />
      </SafeAreaView>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d0e8f2',
    alignItems: 'center',
  },
});

export default App;

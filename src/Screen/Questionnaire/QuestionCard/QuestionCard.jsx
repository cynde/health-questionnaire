import React from 'react';
import {TouchableOpacity, Text, StyleSheet, Image} from 'react-native';
import {Card} from 'react-native-card-stack-swiper';
import RadioButton from '../../../Components/Form/RadioButton';
import NumberInput from '../../../Components/Form/NumberInput';
import DatePicker from '../../../Components/Form/DatePicker';
import TextArea from '../../../Components/Form/TextArea';
import Commons from '../../../Constants/Commons';
import Messages from '../../../Constants/Messages';
import {connect} from 'react-redux';

const {MULTIPLE, NUMBER, DATE, TEXT, YES, HAS_TEMPERATURE_OVER_100} = Commons;
const {TEMPERATURE_OVER_100_MESSAGE} = Messages;

const QuestionCard = ({data, answers}) => {
  const {question, type, choices, unit, fieldName} = data;

  const renderAnswer = () => {
    if (type === MULTIPLE) {
      return choices.map((choice, index) => {
        return (
          <RadioButton key={index} choice={choice} fieldName={fieldName} />
        );
      });
    }
    if (type === NUMBER) {
      return <NumberInput unit={unit} />;
    }
    if (type === DATE) {
      return <DatePicker />;
    }
    if (type === TEXT) {
      return <TextArea />;
    }
  };

  const handleNextPress = () => {
    if (
      answers.hasTemperatureOver100 === YES &&
      fieldName === HAS_TEMPERATURE_OVER_100
    ) {
      alert(TEMPERATURE_OVER_100_MESSAGE);
    }
    this.swiper.swipeLeft();
  };

  return (
    <Card style={styles.card}>
      <Image
        style={styles.cardImage}
        source={require('../../../Assets/home.png')}
      />
      <Text style={styles.question}>{question}</Text>
      {renderAnswer()}
      <TouchableOpacity style={styles.nextButton} onPress={handleNextPress}>
        <Text style={styles.nextButtonText}>Next</Text>
      </TouchableOpacity>
    </Card>
  );
};

const styles = StyleSheet.create({
  card: {
    height: 570,
    width: 320,
    padding: 20,
    paddingTop: 65,
    backgroundColor: '#fcf8ec',
    borderRadius: 20,
    shadowColor: '#456268',
    shadowOffset: {width: 7, height: 7},
    shadowOpacity: 0.1,
  },
  question: {
    color: '#456268',
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 15,
  },
  cardImage: {
    width: 150,
    height: 120,
    position: 'absolute',
    zIndex: 1,
    top: -80,
    left: '30%',
  },
  nextButton: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 120,
    height: 50,
    padding: 12,
    borderTopLeftRadius: 10,
    borderBottomRightRadius: 20,
    justifyContent: 'center',
    backgroundColor: '#79a3b1',
  },
  nextButtonText: {
    color: 'white',
    textAlign: 'center',
  },
});

const mapStateToProps = ({answers}) => {
  return {answers};
};

export default connect(mapStateToProps)(QuestionCard);

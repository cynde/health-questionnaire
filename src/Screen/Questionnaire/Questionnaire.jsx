import React from 'react';
import {StyleSheet, Image, ScrollView} from 'react-native';
import CardStack from 'react-native-card-stack-swiper';
import QuestionCard from './QuestionCard/QuestionCard';
import SummaryCard from './SummaryCard/SummaryCard';
import {connect} from 'react-redux';
import Commons from '../../Constants/Commons';

const {NO, SKIP_FOUR_QUESTIONS} = Commons;

const Questionnaire = ({questions, answers, skipFourQuestions}) => {
  return (
    <ScrollView style={styles.wrapper}>
      <CardStack
        style={styles.stack}
        verticalSwipe={false}
        horizontalSwipe={false}
        renderNoMoreCards={() => <SummaryCard />}
        onSwiped={() => {
          if (answers.hasAllSymptoms === NO) {
            skipFourQuestions();
          }
        }}
        ref={(swiper) => {
          this.swiper = swiper;
        }}>
        {questions.map((question, index) => (
          <QuestionCard key={index} data={question} />
        ))}
      </CardStack>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    overflow: 'visible',
  },
  stack: {
    marginTop: 115,
  },
});

const mapStateToProps = ({questions, answers}) => {
  return {questions, answers};
};

const mapDispatchToProps = (dispatch) => {
  return {
    skipFourQuestions: () => dispatch({type: SKIP_FOUR_QUESTIONS}),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Questionnaire);

import React from 'react';
import {Text, StyleSheet, ScrollView, Image} from 'react-native';
import {Card} from 'react-native-card-stack-swiper';
import {connect} from 'react-redux';

const SummaryCard = ({answers}) => {
  const {
    hasAllSymptoms,
    hasTemperatureOver100,
    hasCough,
    hasSoreThroat,
    shortnessOfBreath,
    amountOfSputum,
    appointmentDate,
    notes,
  } = answers;

  var dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  const renderEachSymptoms = () => {
    return (
      <>
        <Text style={styles.summaryLabel}>Temperature over 100F:</Text>
        <Text style={styles.summaryAnswer}>{hasTemperatureOver100}</Text>
        <Text style={styles.summaryLabel}>Cough:</Text>
        <Text style={styles.summaryAnswer}>{hasCough}</Text>
        <Text style={styles.summaryLabel}>Sore Throat:</Text>
        <Text style={styles.summaryAnswer}>{hasSoreThroat}</Text>
      </>
    );
  };

  return (
    <Card style={styles.summaryCard}>
      <Image
        style={styles.cardImage}
        source={require('../../../Assets/home.png')}
      />
      <Text style={styles.cardTitle}>See you on the next appointment.</Text>
      <ScrollView style={styles.summaryWrapper}>
        <Text style={styles.summaryTitle}>Summary:</Text>
        <Text style={styles.summaryLabel}>
          Temperature over 100F / Cough / Sore Throat:
        </Text>
        <Text style={styles.summaryAnswer}>{hasAllSymptoms}</Text>
        {answers.hasAllSymptoms === 'Yes' && renderEachSymptoms()}
        <Text style={styles.summaryLabel}>
          Shortness of breath when at rest:
        </Text>
        <Text style={styles.summaryAnswer}>{shortnessOfBreath}</Text>
        <Text style={styles.summaryLabel}>Sputum Quantity:</Text>
        <Text style={styles.summaryAnswer}>{amountOfSputum} ml</Text>
        <Text style={styles.summaryLabel}>Appointment Date:</Text>
        <Text style={styles.summaryAnswer}>
          {appointmentDate.toLocaleDateString('en-US', dateOptions)}
        </Text>
        <Text style={styles.summaryLabel}>Notes:</Text>
        <Text style={styles.summaryAnswer}>{notes || '-'}</Text>
      </ScrollView>
    </Card>
  );
};

const styles = StyleSheet.create({
  summaryCard: {
    height: 570,
    paddingTop: 65,
    width: 320,
    padding: 20,
    backgroundColor: '#fcf8ec',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#456268',
    shadowOffset: {width: 7, height: 7},
    shadowOpacity: 0.1,
  },
  cardTitle: {
    color: '#456268',
    fontWeight: 'bold',
    fontSize: 17,
    marginBottom: 15,
    textAlign: 'center',
  },
  cardImage: {
    width: 150,
    height: 120,
    position: 'absolute',
    zIndex: 1,
    top: -80,
    left: '30%',
  },
  summaryWrapper: {
    borderColor: '#79a3b1',
    borderWidth: 1,
    borderRadius: 5,
    padding: 13,
    marginLeft: 10,
    marginRight: 10,
  },
  summaryTitle: {
    fontWeight: 'bold',
    color: '#456268',
    marginBottom: 10,
  },
  summaryLabel: {
    color: '#456268',
    fontSize: 12,
  },
  summaryAnswer: {
    marginLeft: 12,
    marginTop: 5,
    marginBottom: 12,
  },
});

const mapStateToProps = ({answers}) => {
  return {answers};
};

export default connect(mapStateToProps)(SummaryCard);

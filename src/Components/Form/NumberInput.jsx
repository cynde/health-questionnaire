import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {Input, Item} from 'native-base';
import {connect} from 'react-redux';
import Commons from '../../Constants/Commons';

const {CHANGE_DATA, AMOUNT_OF_SPUTUM} = Commons;

const NumberInput = ({unit, amountOfSputum, handleInputChange}) => {
  return (
    <Item style={styles.wrapper}>
      <Input
        style={styles.number}
        keyboardType={'numeric'}
        placeholder={'amount'}
        onChangeText={(value) => handleInputChange(AMOUNT_OF_SPUTUM, value)}
        value={amountOfSputum}
      />
      <Text style={styles.unit}>{unit}</Text>
    </Item>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    width: '40%',
  },
  number: {
    fontSize: 14,
  },
  unit: {
    color: '#ec7b6a',
    fontWeight: 'bold',
  },
});

const mapStateToProps = ({answers}) => {
  return {amountOfSputum: answers.amountOfSputum};
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleInputChange: (fieldName, fieldValue) =>
      dispatch({
        type: CHANGE_DATA,
        fieldName: fieldName,
        fieldValue: fieldValue,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NumberInput);

import React, {useState} from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import {TouchableOpacity, Platform, StyleSheet, Text} from 'react-native';
import {connect} from 'react-redux';
import Commons from '../../Constants/Commons';

const {CHANGE_DATA, APPOINTMENT_DATE} = Commons;

const DatePicker = ({appointmentDate, handleDateChange}) => {
  const [isShow, setIsShow] = useState(false);

  var dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  return (
    <>
      <Text style={styles.boldText}>
        {appointmentDate.toLocaleDateString('en-US', dateOptions)}
      </Text>
      <TouchableOpacity style={styles.button} onPress={() => setIsShow(true)}>
        <Text style={styles.smallText}>Select Date</Text>
      </TouchableOpacity>
      {isShow && (
        <DateTimePicker
          value={appointmentDate}
          minimumDate={new Date()}
          textColor={'black'}
          mode={'date'}
          onChange={(event, selectedDate) => {
            setIsShow(Platform.OS === 'ios');
            handleDateChange(APPOINTMENT_DATE, selectedDate);
          }}
        />
      )}
    </>
  );
};

const styles = StyleSheet.create({
  button: {
    marginTop: 15,
  },
  smallText: {
    color: '#456268',
    fontSize: 13,
    marginLeft: 10,
  },
  boldText: {
    color: '#456268',
    fontSize: 15,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: 15,
  },
});

const mapStateToProps = ({answers}) => {
  return {appointmentDate: answers.appointmentDate};
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleDateChange: (fieldName, fieldValue) =>
      dispatch({
        type: CHANGE_DATA,
        fieldName: fieldName,
        fieldValue: fieldValue,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DatePicker);

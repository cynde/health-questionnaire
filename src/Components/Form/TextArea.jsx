import React from 'react';
import {Textarea} from 'native-base';
import {connect} from 'react-redux';
import Commons from '../../Constants/Commons';

const {CHANGE_DATA, NOTES} = Commons;

const TextArea = ({notes, handleTextAreaChange}) => {
  return (
    <Textarea
      rowSpan={10}
      bordered
      placeholder={'Enter notes here'}
      onChangeText={(value) => handleTextAreaChange(NOTES, value)}
      value={notes}
    />
  );
};

const mapStateToProps = ({answers}) => {
  return {notes: answers.notes};
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleTextAreaChange: (fieldName, fieldValue) =>
      dispatch({
        type: CHANGE_DATA,
        fieldName: fieldName,
        fieldValue: fieldValue,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TextArea);

import React from 'react';
import {Text} from 'react-native';
import {ListItem, Left, Right, Radio} from 'native-base';
import {connect} from 'react-redux';
import Commons from '../../Constants/Commons';

const {CHANGE_DATA} = Commons;

const RadioButton = ({choice, fieldName, answers, handleOnPress}) => {
  return (
    <ListItem
      selected={answers[fieldName] === choice.value}
      onPress={() => handleOnPress(fieldName, choice.value)}>
      <Left>
        <Text>{choice.value}</Text>
      </Left>
      <Right>
        <Radio
          selectedColor={'#ec7b6a'}
          selected={answers[fieldName] === choice.value}
          onPress={() => handleOnPress(fieldName, choice.value)}
        />
      </Right>
    </ListItem>
  );
};

const mapStateToProps = ({answers}) => {
  return {answers};
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleOnPress: (fieldName, fieldValue) =>
      dispatch({
        type: CHANGE_DATA,
        fieldName: fieldName,
        fieldValue: fieldValue,
      }),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RadioButton);

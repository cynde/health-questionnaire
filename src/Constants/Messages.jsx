const Messages = {
  TEMPERATURE_OVER_100_MESSAGE:
    'For emergency situation, please call 911 immediately.',
};

export default Messages;

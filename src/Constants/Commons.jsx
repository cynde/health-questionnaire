const Commons = {
  CHANGE_DATA: 'CHANGE_DATA',
  AMOUNT_OF_SPUTUM: 'amountOfSputum',
  NOTES: 'notes',
  APPOINTMENT_DATE: 'appointmentDate',
  MULTIPLE: 'multiple',
  NUMBER: 'number',
  DATE: 'date',
  TEXT: 'text',
  YES: 'Yes',
  NO: 'No',
  SKIP_FOUR_QUESTIONS: 'SKIP_FOUR_QUESTIONS',
  HAS_TEMPERATURE_OVER_100: 'hasTemperatureOver100',
};

export default Commons;

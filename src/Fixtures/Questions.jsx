const allQuestions = [
  {
    question:
      'Do you have any of these symptoms?\nTemperature over 100F\nCough\nSore Throat',
    type: 'multiple',
    unit: '',
    choices: [
      {
        value: 'Yes',
        action: 'NONE',
        default: false,
      },
      {
        value: 'No',
        action: 'SKIP_5',
        default: true,
      },
    ],
    fieldName: 'hasAllSymptoms',
  },
  {
    question: 'Do you have the following symptoms?\nTemperature over 100F?',
    type: 'multiple',
    unit: '',
    choices: [
      {
        value: 'Yes',
        action: 'ALERT_"For emergency situation, please call 911 immediately."',
        default: false,
      },
      {
        value: 'No',
        action: 'NONE',
        default: true,
      },
    ],
    fieldName: 'hasTemperatureOver100',
  },
  {
    question: 'Do you have the following symptoms?\nCough',
    type: 'multiple',
    unit: '',
    choices: [
      {
        value: 'Yes',
        action: 'NONE',
        default: false,
      },
      {
        value: 'No',
        action: 'NONE',
        default: true,
      },
    ],
    fieldName: 'hasCough',
  },
  {
    question: 'Do you have the following symptoms?\nSore Throat',
    type: 'multiple',
    unit: '',
    choices: [
      {
        value: 'Yes',
        action: 'NONE',
        default: false,
      },
      {
        value: 'No',
        action: 'NONE',
        default: true,
      },
    ],
    fieldName: 'hasSoreThroat',
  },
  {
    question:
      'Breathlessness - Please rate your shortness of breath when at rest on a scale of 0 to 10, with 0 being no shortness of breath and 10 being the worst shortness of breath you can imagine',
    type: 'multiple',
    unit: '',
    choices: [
      {
        value: '0 - None',
        action: 'NONE',
        default: true,
      },
      {
        value: '0.5 - Very Mild',
        action: 'NONE',
        default: false,
      },
      {
        value: '1 - Less Mild',
        action: 'NONE',
        default: false,
      },
      {
        value: '2 - Mild',
        action: 'NONE',
        default: false,
      },
    ],
    fieldName: 'shortnessOfBreath',
  },
  {
    question:
      'Sputum Quantity - Please estimate the total amount of sputum (mucus) you have coughed up in the last 24 hours',
    type: 'number',
    unit: 'ml',
    choices: [],
    fieldName: 'amountOfSputum',
  },
  {
    question: 'Please make an appointment for your next visit',
    type: 'date',
    unit: '',
    choices: [],
    fieldName: 'appointmentDate',
  },
  {
    question: 'Notes',
    type: 'text',
    unit: '',
    choices: [],
    fieldName: 'notes',
  },
];

const noSymptomsQuestions = [
  {
    question:
      'Do you have any of these symptoms?\nTemperature over 100F\nCough\nSore Throat',
    type: 'multiple',
    unit: '',
    choices: [
      {
        value: 'Yes',
        action: 'NONE',
        default: false,
      },
      {
        value: 'No',
        action: 'SKIP_5',
        default: true,
      },
    ],
    fieldName: 'hasAllSymptoms',
  },
  {
    question:
      'Breathlessness - Please rate your shortness of breath when at rest on a scale of 0 to 10, with 0 being no shortness of breath and 10 being the worst shortness of breath you can imagine',
    type: 'multiple',
    unit: '',
    choices: [
      {
        value: '0 - None',
        action: 'NONE',
        default: true,
      },
      {
        value: '0.5 - Very Mild',
        action: 'NONE',
        default: false,
      },
      {
        value: '1 - Less Mild',
        action: 'NONE',
        default: false,
      },
      {
        value: '2 - Mild',
        action: 'NONE',
        default: false,
      },
    ],
    fieldName: 'shortnessOfBreath',
  },
  {
    question:
      'Sputum Quantity - Please estimate the total amount of sputum (mucus) you have coughed up in the last 24 hours',
    type: 'number',
    unit: 'ml',
    choices: [],
    fieldName: 'amountOfSputum',
  },
  {
    question: 'Please make an appointment for your next visit',
    type: 'date',
    unit: '',
    choices: [],
    fieldName: 'appointmentDate',
  },
  {
    question: 'Notes',
    type: 'text',
    unit: '',
    choices: [],
    fieldName: 'notes',
  },
];

export default {
  allQuestions,
  noSymptomsQuestions,
};

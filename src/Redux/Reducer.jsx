import Questions from '../Fixtures/Questions';
import Commons from '../Constants/Commons';

const {allQuestions, noSymptomsQuestions} = Questions;
const {CHANGE_DATA, SKIP_FOUR_QUESTIONS, NO} = Commons;

const initialState = {
  questions: allQuestions,
  answers: {
    hasAllSymptoms: NO,
    hasTemperatureOver100: NO,
    hasCough: NO,
    hasSoreThroat: NO,
    shortnessOfBreath: '0 - None',
    amountOfSputum: '0',
    appointmentDate: new Date(),
    notes: '',
  },
};

const formReducer = (state = initialState, action) => {
  if (action.type === CHANGE_DATA) {
    const newAnswers = {...state.answers};
    newAnswers[action.fieldName] = action.fieldValue;
    const newState = {...state, answers: newAnswers};
    return newState;
  }
  if (action.type === SKIP_FOUR_QUESTIONS) {
    const newState = {...state};
    newState.questions = noSymptomsQuestions;
    return newState;
  }
  return state;
};

export default formReducer;
